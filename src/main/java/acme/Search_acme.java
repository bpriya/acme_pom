package acme;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class Search_acme extends ProjectMethods{

	public Search_acme() {
       PageFactory.initElements(driver, this);
	}

	
	@FindBy(how=How.ID,using="vendorTaxID") WebElement eleVenID;
	@FindBy(how=How.XPATH,using="//button[text()='Search']") WebElement eleSearchbtn;
	public Search_acme enterVendorID(String id) {
		
		clearAndType(eleVenID, id);
		return new Search_acme();
	}
	public Search_Result clickSearch() {
			click(eleSearchbtn);
			return new Search_Result();
		
	}
}
















