package comframework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

import acme.Login_acme;

public class TC001_LoginLogout extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName="TC001_Acme_Search";
		testDescription="SearchResult";
		testNodes="Leads";
		author="Banu";
		category="smoke";
		dataSheetName="acmedata";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String username, String password,String id) {
		new Login_acme().enterUsername(username)
		.enterPassword(password).clickLogin().clickSearch().enterVendorID(id).clickSearch().getResult();
	}
}
