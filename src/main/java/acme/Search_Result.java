package acme;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class Search_Result extends ProjectMethods{

	public Search_Result() {
       PageFactory.initElements(driver, this);
	}

	
	
	@FindBy(how=How.XPATH,using="//td[text()='RO212121']/preceding-sibling::td") WebElement eleVendortxt;
	public Search_Result getResult() {
		getElementText(eleVendortxt);
		return new Search_Result();
	}
	
	
}
















