package acme;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class Home_acme extends ProjectMethods{

	public Home_acme() {
       PageFactory.initElements(driver, this);
	}

	
	@FindBy(how=How.XPATH,using="//button[text()=' Vendors']") WebElement elevendor;
	@FindBy(how=How.LINK_TEXT,using="Search for Vendor") WebElement elesearch;
	public Search_acme clickSearch() {
		performAction(elevendor, elesearch);
		return new Search_acme(); 
	}      


}
















