package acme;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;
import com.framework.pages.HomePage;
import com.framework.pages.LoginPage;

public class Login_acme extends ProjectMethods{
	
	public Login_acme()
	{
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how = How.ID,using="email") WebElement eleUsername;
	@FindBy(how = How.ID,using="password") WebElement elePassword;
	@FindBy(how = How.ID,using="buttonLogin") WebElement eleLogin;
	
	public Login_acme enterUsername(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, data);
		return this; 
	}
	public Login_acme enterPassword(String data) {
		//WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, data);
		return this;
	}
	public Home_acme clickLogin() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
	    click(eleLogin);
	    /*HomePage hp = new HomePage();
	    return hp;*/ 
	    return new Home_acme();
	}
}

